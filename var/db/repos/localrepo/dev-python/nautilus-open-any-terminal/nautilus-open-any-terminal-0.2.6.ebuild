# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{6,7,8} )
inherit distutils-r1

DESCRIPTION="Open terminal with Nautilus"
HOMEPAGE="https://github.com/Stunkymonkey/nautilus-open-any-terminal"
SRC_URI="https://github.com/Stunkymonkey/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="gnome-base/nautilus
		dev-python/nautilus-python
		dev-python/setuptools
"
RDEPEND="${DEPEND}"
BDEPEND=""



