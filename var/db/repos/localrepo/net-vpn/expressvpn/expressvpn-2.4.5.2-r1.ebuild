# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit bash-completion-r1 desktop systemd rpm unpacker

MY_P="expressvpn-2.4.5.2-1"

DESCRIPTION="Propietary VPN client for Linux"
HOMEPAGE="https://expressvpn.com"
BASE_URI="https://download.expressvpn.xyz/clients/linux"
SRC_URI="
amd64? ( "${BASE_URI}/${MY_P}-x86_64.pkg.tar.xz" )
"

LICENSE="ExpressVPN"
SLOT="0"
KEYWORDS="~amd64 ~arm ~x86"
RESTRICT="mirror strip"
IUSE="systemd"

DEPEND="sys-apps/net-tools
systemd? ( sys-apps/systemd )"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_install() {
  mkdir -p "${ED}/usr/sbin"
  cp -ar ./usr/bin "${ED}/usr/"
  cp -ar ./usr/bin "${ED}/usr/sbin"
  cp -ar ./usr/lib "${ED}/usr/lib"
  doman ./usr/share/man/man1/expressvpn.1
  domenu ./usr/lib/${PN}/${PN}-agent.desktop
  newicon ./usr/lib/${PN}/icon.png ${PN}-agent.icon
  newbashcomp ./usr/lib/${PN}/bash-completion ${PN}
  newinitd "${FILESDIR}/${PN}.init" ${PN}
  use systemd && systemd_dounit usr/lib/${PN}/${PN}.service
}
