# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

DESCRIPTION="Google Test - C++ testing utility"
HOMEPAGE="https://github.com/google/googletest"
SRC_URI="https://github.com/google/${PN}/archive/release-${PV}.tar.gz -> ${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-util/cmake"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/${PN}-release-${PV}"
