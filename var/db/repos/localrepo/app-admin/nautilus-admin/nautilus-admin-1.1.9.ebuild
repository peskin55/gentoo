# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake-utils

DESCRIPTION=""
HOMEPAGE="https://github.com/brunonova/nautilus-admin"
SRC_URI="https://github.com/brunonova/${PN}/releases/download/v${PV}/${PN}_${PV}.tar.xz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="gnome-base/nautilus
	dev-python/nautilus-python
	sys-auth/polkit
	dev-util/cmake
	sys-devel/gettext
"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}
