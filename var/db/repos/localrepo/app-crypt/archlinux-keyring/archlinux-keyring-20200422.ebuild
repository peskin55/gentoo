# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arch Linux PGP keyring"
HOMEPAGE="https://projects.archlinux.org/archlinux-keyring.git/"
SRC_URI="https://sources.archlinux.org/other/${PN}/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-devel/make"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
        sed -i 's/PREFIX = \/usr\/local/PREFIX = \/usr/g' ${S}/Makefile
        eapply_user
}

src_configure() { :; }

src_compile() { :; }

src_install() {
	if [[ -f Makefile ]] || [[ -f GNUmakefile ]] || [[ -f makefile ]] ; then
		emake DESTDIR="${D}" install
	fi
}
