# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arch Linux build source file management tool"
HOMEPAGE="https://github.com/falconindy/asp"
SRC_URI="https://github.com/archlinux/asp/archive/v${PV}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-apps/gawk
		app-shells/bash
		app-misc/jq
		app-arch/libarchive
		app-text/asciidoc
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare() {
	sed -i 's/PREFIX = \/usr\/local/PREFIX = \/usr/g' ${S}/Makefile
	eapply_user
}
