# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Arch Linux mirror list for pacman"
HOMEPAGE="https://archlinux.org/mirrorlist"
SRC_URI="https://archlinux.org/mirrorlist/all -> ${P}.txt"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-apps/util-linux"
RDEPEND="${DEPEND}"
BDEPEND=""

S=${WORKDIR}

src_configure() { :; }

src_compile() { :; }

src_install() {
  insinto /etc/pacman.d
  doins ${FILESDIR}/mirrorlist
}
