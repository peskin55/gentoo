# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit autotools

DESCRIPTION="Small, easy-to-configure FTP server"
HOMEPAGE="http://bftpd.sourceforge.net"
SRC_URI="https://astuteinternet.dl.sourceforge.net/project/${PN}/${PN}/${P}/${P}.tar.gz"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-libs/pam
		sys-libs/zlib
		sys-libs/glibc
		app-admin/logrotate
"
RDEPEND="${DEPEND}
				 net-ftp/ftpbase
				 sys-apps/xinetd
"
BDEPEND=""

S=${WORKDIR}/${PN}

src_prepare() {
	eapply ${FILESDIR}/${PN}-5.5-Makefile.patch
	eapply_user
	sed 's|#ROOTDIR="/path/for/anonymous/user"|# bftpd interprets ROOTDIR="%h" (the default), as ROOTDIR="/" for the anonymous user, override it\n  ROOTDIR="/srv/ftp"|' -i ${S}/bftpd.conf
	rm -f ${S}/config.{cache,status}
	eautoreconf
}

src_configure() {
	econf \
		--enable-pam \
		--enable-libz
}

src_compile() {
	if [ -f Makefile ] || [ -f GNUmakefile ] || [ -f makefile ]; then
		emake || die "emake failed"
	fi
}

src_install() {
	if [[ -f Makefile ]] || [[ -f GNUmakefile ]] || [[ -f makefile ]] ; then
		emake DESTDIR="${D}" install
	fi
	rm -rf ${D}/usr/var
	insinto /etc/logrotate.d
	newins ${FILESDIR}/${PN}.logrotate ${PN}
	insinto /etc/pam.d
	newins ${FILESDIR}/${PN}.pam ${PN}
	insinto /etc/xinetd.d
	newins ${FILESDIR}/${PN}.xinetd ${PN}
	newinitd ${FILESDIR}/${PN}.init ${PN}
}
