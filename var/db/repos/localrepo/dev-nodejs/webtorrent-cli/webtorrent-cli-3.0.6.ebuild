# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The streaming torrent client. For the command line."
HOMEPAGE="https://wentorrent.io"
SRC_URI="https://github.com/webtorrent/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="net-libs/nodejs"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/npm-build"

pkg_pretend() {
	# shellcheck disable=SC2086
	if has network-sandbox ${FEATURES} && [[ "${MERGE_TYPE}" != binary ]]; then
		ewarn
		ewarn "${CATEGORY}/${PN} requires 'network-sandbox' to be disabled in FEATURES"
		ewarn
		die "[network-sandbox] is enabled in FEATURES"
	fi
}

src_unpack() {
	mkdir -p npm-build/usr || die
}

src_compile() {
	npm install -g --prefix ./usr "${DISTDIR}/${P}.tar.gz" || die
}

src_install() {
	# Dedup
	cp -ar "${WORKDIR}"/npm-build/* "${ED}" || die
}
